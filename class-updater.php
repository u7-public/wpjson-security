<?php

defined('ABSPATH') or die('Cheatin\' huh?');

if (!class_exists('WebsiteAdminSecurity_Updater')) {
    class WebsiteAdminSecurity_Updater {

        private $gitlab_repo = 'u7-public/wpjson-security';
        private $gitlab_repo_url;
        private $version;

        public function __construct() {
        
            $this->version = WebsiteAdminSecurity::get_version();
            $this->gitlab_repo_url = 'https://gitlab.com/' . $this->gitlab_repo;

            // correctly updated 4
            // !This line means the update worked
            // delete_site_transient('update_plugins');

            add_filter( 'pre_set_site_transient_update_plugins', [ $this, 'check_for_updates' ] );
            add_filter('upgrader_post_install', [$this, 'rename_gitlab_plugin_directory'], 10, 3);
        }

        public function check_for_updates( $transient ) {

            if ( empty( $transient->checked ) ) {
                return $transient;
            }

            $update = $this->get_newer_version();
            update_option( 'website_admin_security_new_version', $update); // set update

            if( $update ) {
                $transient->response['wpjson-security/website-admin-security.php'] = (object) array(
                    'slug'        => 'wpjson-security',
                    'new_version' => $update,
                    'package'     => $this->gitlab_repo_url . '/-/archive/' . $update . '/wpjson-security-' . $update . '.zip',
                    'url'         => $this->gitlab_repo_url,
                );
            }

            return $transient;
        }

        private function get_newer_version() {

            $url = 'https://gitlab.com/api/v4/projects/'.urlencode($this->gitlab_repo).'/releases';
        
            $args = array(
                'headers' => array(
                    'Accept' => 'application/json',
                ),
            );
            
            $response = wp_remote_get( $url, $args );
        
            if( is_wp_error( $response ) ) {
                return false;
            }
        
            $releases = json_decode( wp_remote_retrieve_body( $response ) );
            $current_plugin_version = $this->version;  // Get current version
        
            foreach( $releases as $release ) {
                if ( version_compare( $release->tag_name, $current_plugin_version, '>' ) ) {
                    return $release->tag_name;
                }
            }
        
            return false;
        }

        public function custom_log($message) {
            // ignore this in production
            if (WAS_IS_PRODUCTION) {
                return;
            }
        
            $log_directory = WP_CONTENT_DIR . '/logs'; // Creating a logs directory in wp-content
            if (!is_dir($log_directory)) {
                mkdir($log_directory, 0755, true);
            }
        
            $log_file = $log_directory . '/custom_log.txt';
            $current_time = date('Y-m-d H:i:s');
            if (false === file_put_contents($log_file, $current_time . ' - ' . $message . PHP_EOL, FILE_APPEND)) {
                die('Plugin logging is not operational');
            }
        }

        public function rename_gitlab_plugin_directory($response, $hook_extra, $result) {
            $this->custom_log('Entered rename_gitlab_plugin_directory');
        
            // Check if the updating plugin is 'wpjson-security'
            if (!isset($hook_extra['plugin']) || 'wpjson-security/website-admin-security.php' != $hook_extra['plugin']) {
                $this->custom_log('Exiting early: Not updating wpjson-security plugin');
                return $response;
            }
        
            // Define the desired slug (directory name without version)
            $desired_slug = 'wpjson-security';
        
            // Source is where WordPress has installed the plugin
            $source = trailingslashit($result['destination']);
        
            // Destination is where you want the plugin to reside
            $destination = preg_replace('/-[\d.]+(?=\/$)/', '', $source); // strip out the version number from the source
        
            // Log paths for debugging
            $this->custom_log('Source: ' . $source);
            $this->custom_log('Destination: ' . $destination);
        
            // Rename the directory
            if (@rename($source, $destination)) {
                $this->custom_log('Directory renamed successfully');
            } else {
                $this->custom_log('Failed to rename directory');
            }
        
            return $response;
        }
    }
}
<?php
/*
Plugin Name: Website Admin Security
Description: This plugin provides additional security features to restrict public access to user data via WP-JSON
Version: 1.0.2
Author: U7
Author URI: http://u7solutions.com/
*/

defined('ABSPATH') or die('Cheatin\' huh?');

// Include the necessary classes
require_once plugin_dir_path( __FILE__ ) . 'class-updater.php';
require_once plugin_dir_path( __FILE__ ) . 'class-security.php';

// Setup plugin definitions
if (!defined('WAS_IS_PRODUCTION')) {
    define('WAS_IS_PRODUCTION', true);
}

if (!class_exists('WebsiteAdminSecurity')) {
    // Main plugin class
    class WebsiteAdminSecurity {
        private static $instance;

        private function __construct() {
            new WebsiteAdminSecurity_Updater();
            new WebsiteAdminSecurity_Security();
        }

        public static function get_instance() {
            if ( null === self::$instance ) {
                self::$instance = new self();
            }
            return self::$instance;
        }

        public static function get_version() {
            if (!function_exists('get_plugin_data')) {
                require_once(ABSPATH . 'wp-admin/includes/plugin.php');
            }

            $plugin_data = get_plugin_data(__FILE__, false, false);
            return $plugin_data['Version'];
        }
    }
}

// Initialize the plugin
add_action( 'init', array( 'WebsiteAdminSecurity', 'get_instance' ) );
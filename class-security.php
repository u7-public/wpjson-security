<?php

defined('ABSPATH') or die('Cheatin\' huh?');

if (!class_exists('WebsiteAdminSecurity_Security')) {

    class WebsiteAdminSecurity_Security {
        public function __construct() {
            if ( !current_user_can( 'manage_options' ) ) {
                add_filter( 'rest_endpoints', array( $this, 'disable_users_endpoints' ) );
                add_filter( 'rest_user_collection_params', array( $this, 'remove_author_param' ), 10, 1 );
                add_filter( 'rest_user_query', array( $this, 'restrict_author_param' ), 10, 2 );
            }
        }
    
        public function disable_users_endpoints( $endpoints ) {
            if ( isset( $endpoints['/wp/v2/users'] ) ) {
                unset( $endpoints['/wp/v2/users'] );
            }
            if ( isset( $endpoints['/wp/v2/users/(?P<id>[\d]+)'] ) ) {
                unset( $endpoints['/wp/v2/users/(?P<id>[\d]+)'] );
            }
            return $endpoints;
        }
    
        public function remove_author_param( $params ) {
            if ( isset( $params['author'] ) ) {
                unset( $params['author'] );
            }
            return $params;
        }
    
        public function restrict_author_param( $prepared_args, $request ) {
            if ( ! current_user_can( 'list_users' ) ) {
                $prepared_args['author__in'] = array( get_current_user_id() );
            }
            return $prepared_args;
        }
    }

}